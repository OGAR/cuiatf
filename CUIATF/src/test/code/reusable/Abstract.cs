﻿using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UITesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF
{
    public class Abstract : CodeUIPageObject
    {
        #region Constructor

        public Abstract(UITestControl uiParent, string url) : base(uiParent, url)
        {
            
        }
        public Abstract(UITestControl uiParent, string url, Dictionary<string,string> tcMap) : base(uiParent, url, tcMap)
        {
            
        }

        public Abstract(BrowserWindow uiParentB, string url) : base(uiParentB, url)
        {

        }
        public Abstract(BrowserWindow uiParentB, string url, Dictionary<string, string> tcMap) : base(uiParentB, url, tcMap)
        {

        }
        #endregion
    }
}
