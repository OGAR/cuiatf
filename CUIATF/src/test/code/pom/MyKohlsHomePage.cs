﻿using CUIATF.src.main.code.reusable;
using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UITesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF.src.test.code.pom
{
    class MyKohlsHomePage : Abstract
    {
        UIElement linkMyDepartment;
        UITextArea searchControl;
        UIHyperLink linkCommunity;
       

        #region Constructor
        public MyKohlsHomePage(UITestControl uiParent, string url): base(uiParent, url)
        {
            initElements();
        }

        public MyKohlsHomePage(UITestControl uiParent, string url, Dictionary<string,string> tcMap) : base(uiParent, url, tcMap)
        {
            initElements();
        }

        public MyKohlsHomePage(BrowserWindow uiParentB, string url) : base(uiParentB, url)
        {
            initElements();
        }

        public MyKohlsHomePage(BrowserWindow uiParentB, string url, Dictionary<string, string> tcMap) : base(uiParentB, url, tcMap)
        {
            initElements();
        }
        #endregion

        private void initElements()
        {
            linkMyDepartment = new UIElement(_uiParentB, property.getPropertyAUT("homepage_mydepartment"), null);
            linkCommunity = new UIHyperLink(_uiParentB, property.getPropertyAUT("homepage_communityLink"), null);
            searchControl = new UITextArea(_uiParentB, property.getPropertyAUT("kohlshome_textArea_searchControl"), null);
        }

        public void clickLink()
        {
            linkMyDepartment.click();
            //linkCommunity.click();
        }
        public void demoActions()
        {
            searchControl.click();
        }
    }
}
