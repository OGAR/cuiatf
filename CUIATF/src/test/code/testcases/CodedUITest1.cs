﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using CUIATF.src.test.code.pom;

namespace CUIATF 
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class CodedUITest1 : CodeUIDriver
    {
        public CodedUITest1()
        {
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            this.UIListener.BeforeTestInitialize(TestContext);
        }

        [TestMethod]
        public void MYK_CodeUITest()
        {            
            CodeUIDriver.getThreadBrowser().NavigateToUrl(new Uri("http://www.kohls.com"));
            MyKohlsHomePage mkh = new MyKohlsHomePage(CodeUIDriver.getThreadBrowser(),CodeUIDriver.getThreadBrowser().Uri.AbsoluteUri,CodeUIDriver.getTestCaseDataSet());
            mkh.demoActions();
        }


        

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            //this.UIListener.AfterTestInitialize(TestContext);            
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public CodeUIListener UIListener
        {
            get
            {
                if ((this.listener == null))
                {
                    this.listener = new CodeUIListener();
                }
                return this.listener;
            }
        }
        private CodeUIListener listener;        
    }
}
