﻿using CUIATF.src.main.code.common;
using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF.src.main.code.reusable
{
    class UIHyperLink : Component
    {
        private static ReadResource resources = new ReadResource();
        public UIHyperLink(UITestControl uiParent, string query, Component parent) : base(uiParent,query,parent)
        {

        }

        public UIHyperLink(BrowserWindow uiParentB, string query, Component parent) : base(uiParentB, query, parent)
        {

        }

        public override void click()
        {              
            string innerText="";
            innerText = "Community";
            var link = new HtmlHyperlink(_uiParentB);
            link.SearchProperties.Add(HtmlHyperlink.PropertyNames.InnerText, innerText);
            Mouse.Click(link);
            //base.click();
        }
    }
}
