﻿using CUIATF.src.main.code.common;
using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF.src.main.code.reusable
{
    class UIButton : Component
    {
        private static ReadResource resources = new ReadResource();
        public UIButton(UITestControl uiParent, string query, Component parent) : base(uiParent, query, parent)
        {

        }

        public UIButton(BrowserWindow uiParentB, string query, Component parent) : base(uiParentB, query, parent)
        {

        }

        public override void click()
        {
            Console.WriteLine("Executing the override Click method");
            string displayText = resources.getPropertyAUT("homepage_button_mydepartment");
            var link = new HtmlButton(_uiParentB);
            link.SearchProperties.Add(HtmlButton.PropertyNames.DisplayText, displayText);
            Mouse.Click(link);
            //base.click();
        }
    }
}
