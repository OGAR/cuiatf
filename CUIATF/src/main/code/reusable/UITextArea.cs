﻿using CUIATF.src.main.code.common;
using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF.src.main.code.reusable
{
    class UITextArea : Component
    {
        private static ReadResource resources = new ReadResource();
        public UITextArea(UITestControl uiParent, string query, Component parent) : base(uiParent,query,parent)
        {

        }

        public UITextArea(BrowserWindow uiParentB, string query, Component parent) : base(uiParentB, query, parent)
        {

        }

        public override void click()
        {
            var textArea = new HtmlTextArea(_uiParentB);
            textArea.SearchProperties.Add(HtmlTextArea.PropertyNames.Id, _query);            
            if (textArea.TryFind())
            {
                Mouse.Click(textArea);
                Keyboard.SendKeys("This is a silly test");
            }
        }
    }
}
