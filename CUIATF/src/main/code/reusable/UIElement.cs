﻿using CUIATF.src.main.code.common;
using Microsoft.VisualStudio.TestTools.UITesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF
{
    public class UIElement : Component
    {

        public UIElement(UITestControl uiParent, string query, Component parent) : base(uiParent,query,parent)
        {

        }

        public UIElement(BrowserWindow uiParentB, string query, Component parent) : base(uiParentB, query, parent)
        {

        }

    }
}
