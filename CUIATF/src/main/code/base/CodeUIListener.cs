﻿using CUIATF.src.main.code.datahandling;
using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CUIATF
{
    public class CodeUIListener 
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private static ReadResource resources = new ReadResource();
      
        /// <summary>
        /// Method and process to be performed to before a test would be executed
        /// Parameters: TestContext would have all the information related to the test case to be on execution
        /// </summary>
        /// <param name="TestContext"></param>
        public void BeforeTestInitialize(TestContext TestContext)
        {            
            CodeUIDriver.setThreadBrowser(this.UIBrowser);
            this.UIBrowser.openBrowser();                                    
            // Setting the runtime path for later usage           
            CodeUIDriver.setRuntimePath(CodeBase(TestContext).Substring(0, CodeBase(TestContext).LastIndexOf("bin")));
            Testcasefeeder tcf = new Testcasefeeder();
            if (tcf.feedTestCase(TestContext))
            {
                Console.WriteLine("Fetching data...");
                // Look for a file that match the value from the resource file
                FetchData fetch = new FetchData(resources.getPropertyAUT(tcf.dataSheetProp(TestContext)));
                // Update the Base class with the Dictionary of values that belong to the test case by name
                CodeUIDriver.setTestCaseDataSet(fetch.mapTestCaseWithData(TestContext.TestName));
                if (CodeUIDriver.getTestCaseDataSet() != null)
                {
                    Console.WriteLine("Dataset properly fetched and attached to the test case base class");
                }
            }
            pageRendered();
        }

        private void pageRendered()
        {
            try
            {
                // Look for the document.readyState                
                Console.WriteLine("Default Page load begin:" + DateTime.Now.ToString("HH:mm:ss tt"));
                while ((this.UIBrowser.ExecuteScript("return (document.readyState)").Equals("loading")))
                {
                    //span of 250 milliseconds for page load check
                    Thread.Sleep(250);
                }
                Console.WriteLine("Default Page load end:" + DateTime.Now.ToString("HH:mm:ss tt"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.StackTrace);
            }
        }

        public void AfterTestInitialize(TestContext TestContext)
        {
            try
            {

                if (CodeUIDriver.getThreadBrowser() != null)
                {
                    if (TestContext.CurrentTestOutcome.Equals("Passed"))
                    {
                        // Test pass 
                    }
                    else
                    {
                        // Test failed, skip, inconclusive, error
                    }
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
            }
        }

        // This will access the BrowserFactory class
        public CodeUIBrowserFactory UIBrowser
        {
            get
            {
                if ((this.sessionBrowser == null))
                {
                    this.sessionBrowser = new CodeUIBrowserFactory();
                }
                return this.sessionBrowser;
            }
        }
        private CodeUIBrowserFactory sessionBrowser;

        private static string CodeBase(TestContext testContext)
        {
            System.Type t = testContext.GetType();
            FieldInfo field = t.GetField("m_test", BindingFlags.NonPublic | BindingFlags.Instance);
            object fieldValue = field.GetValue(testContext);
            t = fieldValue.GetType();
            PropertyInfo property = fieldValue.GetType().GetProperty("CodeBase");
            return (string)property.GetValue(fieldValue, null);
        }
    }
}
