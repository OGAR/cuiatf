﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting;
using System.Diagnostics;

namespace CUIATF
{
    public class CodeUIBrowserFactory : BrowserWindow
    {
        static Process proc = null;

        public void openBrowser(string url)
        {
            BrowserWindow browser = this.pBrowser.createBrowser(url);
            // Attaching browser to process to be reused if desired
            proc = browser.Process;
            browser.CloseOnPlaybackCleanup = false;
        }

        public void openBrowser()
        {
            BrowserWindow browser = this.pBrowser.createBrowser();
            // Attaching browser to process to be reused if desired
            proc = browser.Process;
            browser.CloseOnPlaybackCleanup = false;
        }

        public CodeUIBrowserFactory()
        {            
            this.SearchProperties[BrowserWindow.PropertyNames.ClassName] = "IEFrame";
            this.SearchProperties.Add(new PropertyExpression(BrowserWindow.PropertyNames.ClassName, "IEFrame", PropertyExpressionOperator.Contains));          
        }

        public Browser pBrowser
        {
            get
            {
                if ((this.mBrowser == null))
                {
                    this.mBrowser = new Browser();
                }
                return this.mBrowser;
            }
        }
        private Browser mBrowser;

        
    }

    public class Browser
    {
        // Assuming that the browser is by default IE
        public BrowserWindow createBrowser(string url)
        {
            var browser = BrowserWindow.Launch(url);
            return browser;
        }

        public BrowserWindow createBrowser()
        {
            var browser = BrowserWindow.Launch();
            return browser;
        }
    }

}
