﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting;
using System.Threading;
using AventStack.ExtentReports;
using CUIATF.src.main.utils;
using System.Diagnostics;
using NLog;

namespace CUIATF
{
    public abstract class CodeUIDriver : BrowserWindow
    {

        private static ReadResource resources = new ReadResource();
       
        private static ThreadLocal<BrowserWindow> threadBrowser = new ThreadLocal<BrowserWindow>();
        protected static ThreadLocal<Dictionary<string, string>> testCaseDataSet = new ThreadLocal<Dictionary<string, string>>();
        protected static ThreadLocal<Dictionary<string, string>> dbQueriesSet = new ThreadLocal<Dictionary<string, string>>();
        protected static ThreadLocal<string> stepFlag = new ThreadLocal<string>();
        protected static Process proc = null;
        private static string _testName;
        protected static string runtimePath;
        protected static string websiteURL;
        private static ExtentReports _extent;
        private static ExtentTest _test;

        public CodeUIDriver()
        {

        }

        public static void setRuntimePath(string rtp)
        {
            runtimePath = rtp;
        }

        public static string getRuntimePath()
        {
            return runtimePath;
        }


        public static void setProcess(Process process)
        {
            proc = process;
        }

        public static Process getProcess()
        {
            return proc;
        }
        public static void setExtentIns(ExtentReports instance)
        {
            _extent = instance;
        }

        public static void setTestPass(ExtentTest test, String message)
        {
            test.Log(Status.Pass, message);
        }

        public static void setTestFail(ExtentTest test, String message)
        {
            test.Log(Status.Fail, message);
        }

        public static ExtentTest getTestInstance()
        {
            return _test;

        }

        public static ExtentReports getExtendInstance()
        {
            return _extent;

        }

        public static void setTestName(String name)
        {
            _testName = name;
            _test = _extent.CreateTest(_testName);
        }
        public static String getStepFlag() { return stepFlag.Value; }
        public static void setStepFlag(string val)
        {
            stepFlag.Value = val;
        }

        public static Dictionary<string,string> getDBQuerySet()
        {
            return dbQueriesSet.Value;
        }
        public static void setDBQueySet(Dictionary<string, string> _dbQueriesSet)
        {
            dbQueriesSet.Value = _dbQueriesSet;
        }

        public static BrowserWindow getThreadBrowser()
        {
            return threadBrowser.Value;
        }

        public static void setThreadBrowser(BrowserWindow browser)
        {
            threadBrowser.Value = browser;
            websiteURL = resources.getPropertyAUT("site_url");
        }
        
        public static Dictionary<string,string> getTestCaseDataSet()
        {
            return testCaseDataSet.Value;
        }
        public static void setTestCaseDataSet(Dictionary<string,string> _dataSet)
        {
            if (_dataSet != null)
            {
                testCaseDataSet.Value = _dataSet;
            }else
            {
                Console.WriteLine("The current test case does not have any data associated");
            }

        }
    }
}
