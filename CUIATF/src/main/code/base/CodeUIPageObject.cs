﻿using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UITesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CUIATF
{
    public class CodeUIPageObject
    {
        protected UITestControl _uiParent;
        protected BrowserWindow _uiParentB;    
        protected static Dictionary<string, string> _tcMap = null;
        protected static string _url;

        protected static ReadResource property = new ReadResource();

        #region Constructor

        public CodeUIPageObject(UITestControl uiParent, string url)
        {
            _uiParent = uiParent;
            _url = url;
        }
        public CodeUIPageObject(UITestControl uiParent, string url, Dictionary<string,string> tcMap)
        {
            _uiParent = uiParent;
            _url = url;
            _tcMap = tcMap;
        }

        public CodeUIPageObject(BrowserWindow uiParent, string url)
        {
            _uiParentB = uiParent;
            _url = url;
            pageRendered();
        }
        public CodeUIPageObject(BrowserWindow uiParent, string url, Dictionary<string, string> tcMap)
        {
            _uiParentB = uiParent;
            _url = url;
            _tcMap = tcMap;
            pageRendered();
        }
        #endregion

        private void pageRendered()
        {            
            try
            {                                             
                // Look for the document.readyState                
                Console.WriteLine("Begin: ------------- Page load " + DateTime.Now.ToString("HH:mm:ss tt"));
                while ((_uiParentB.ExecuteScript("return (document.readyState)").Equals("loading")))
                {
                    //span of 250 milliseconds for page load check
                    Thread.Sleep(250);                        
                }
                Console.WriteLine("End: ---------------- Page load " + DateTime.Now.ToString("HH:mm:ss tt"));
                injectJQuery();             
            }catch (Exception e)
            {
                Console.WriteLine("Error: " + e.StackTrace);
            }           
        }

        private void injectJQuery()
        {
            string content = "";
            string sPath = CodeUIDriver.getRuntimePath();
            String projectPath = new Uri(sPath).LocalPath;
            projectPath = projectPath + "src\\main\\resources\\jquery-3.1.0.min.js";
            try
            {
                content = File.ReadAllText(projectPath);                
                _uiParentB.ExecuteScript(content);                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
        public String updateJQuery(String actualJQuery, String objKey)
        {
            String queryStr = "";
            if (actualJQuery.Length > 0)
            {
                try
                {
                    queryStr = actualJQuery.Replace("DEFAULT", getKeyValue(objKey));
                }
                catch (Exception e)
                {
                    Console.WriteLine("=====================================================");
                    Console.WriteLine("Exception, not found key: " + objKey + " in the map...", e.Message);
                    Console.WriteLine("=====================================================");
                    return actualJQuery;
                }

            }
            return queryStr;
        }

        public String updateJQueryUPPER(String actualJQuery, String objKey)
        {
            String queryStr = "";
            if (actualJQuery.Length > 0)
            {
                try
                {
                    queryStr = actualJQuery.Replace("DEFAULT", getKeyValue(objKey).ToUpper());
                }
                catch (Exception e)
                {
                    Console.WriteLine("=====================================================");
                    Console.WriteLine("Exception, not found key: " + objKey + " in the map...", e.Message);
                    Console.WriteLine("=====================================================");
                    return actualJQuery;
                }

            }
            return queryStr;
        }

        private string getKeyValue(string key)
        {
            string value = null;
            _tcMap.TryGetValue(key, out value);
            return value;
        }
    }
}
