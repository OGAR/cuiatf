﻿using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UITesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF
{
    public class PageObject
    {
        protected UITestControl _uiParent;
        protected BrowserWindow _uiParentB;    
        protected static Dictionary<string, string> _tcMap = null;
        protected static string _url;

        protected static ReadResource property = new ReadResource();

        #region Constructor

        public PageObject(UITestControl uiParent, string url)
        {
            _uiParent = uiParent;
            _url = url;
        }
        public PageObject(UITestControl uiParent, string url, Dictionary<string,string> tcMap)
        {
            _uiParent = uiParent;
            _url = url;
            _tcMap = tcMap;
        }

        public PageObject(BrowserWindow uiParent, string url)
        {
            _uiParentB = uiParent;
            _url = url;
            pageRendered();
        }
        public PageObject(BrowserWindow uiParent, string url, Dictionary<string, string> tcMap)
        {
            _uiParentB = uiParent;
            _url = url;
            _tcMap = tcMap;
            pageRendered();
        }
        #endregion

        private void pageRendered()
        {
            //Open JS file 
            string sPath = CodeUIDriver.getRuntimePath();
            String actualPath = sPath.Substring(0, sPath.LastIndexOf("bin"));
            String projectPath = new Uri(actualPath).LocalPath;
            projectPath += "src\\main\\resources\\onLoadPage.js";
            try
            {
                if (File.Exists(projectPath))
                {
                    string content = File.ReadAllText(projectPath);
                    _uiParentB.ExecuteScript(content);
                }
            }catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            
            
        }

        public String updateJQuery(String actualJQuery, String objKey)
        {
            String queryStr = "";
            if (actualJQuery.Length > 0)
            {
                try
                {
                    queryStr = actualJQuery.Replace("DEFAULT", getKeyValue(objKey));
                }
                catch (Exception e)
                {
                    Console.WriteLine("=====================================================");
                    Console.WriteLine("Exception, not found key: " + objKey + " in the map...", e.Message);
                    Console.WriteLine("=====================================================");
                    return actualJQuery;
                }

            }
            return queryStr;
        }

        public String updateJQueryUPPER(String actualJQuery, String objKey)
        {
            String queryStr = "";
            if (actualJQuery.Length > 0)
            {
                try
                {
                    queryStr = actualJQuery.Replace("DEFAULT", getKeyValue(objKey).ToUpper());
                }
                catch (Exception e)
                {
                    Console.WriteLine("=====================================================");
                    Console.WriteLine("Exception, not found key: " + objKey + " in the map...", e.Message);
                    Console.WriteLine("=====================================================");
                    return actualJQuery;
                }

            }
            return queryStr;
        }

        private string getKeyValue(string key)
        {
            string value = null;
            _tcMap.TryGetValue(key, out value);
            return value;
        }
    }
}
