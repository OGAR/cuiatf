﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITest;
using System;
using CodedUI.jQueryExtensions;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using CUIATF.src.main.utils;

namespace CUIATF.src.main.code.common
{
    public class Component
    {
       
        protected UITestControl _uiParent=null;
        protected BrowserWindow _uiParentB = null;
        protected string _query="";
        private string sXpath = "";
        protected Component _parent = null;
        protected string jQueryString = "";

        private static ReadResource resources = new ReadResource();

        #region Constructor
        public Component(UITestControl uiParent, string query, Component parent)
        {
            _uiParent = uiParent;
            _query = query;
            _parent = parent;
        }

        public Component(BrowserWindow uiParent, string query, Component parent)
        {
            _uiParentB = uiParent;
            _query = query;
            _parent = parent;
        }
        #endregion

        #region Approach of JQuery

        private bool waitForElement(string jquery)
        {
            bool bFlag = false;
            string message = "UI Element not located !!!";
            try
            {
                
                Console.WriteLine("Begin ------ looking for: "+ jquery +" @ "+ DateTime.Now.ToString("HH:mm:ss tt"));
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                while (stopwatch.ElapsedMilliseconds < Convert.ToInt16(resources.getPropertyAUT("autUITimeout")))
                {                    
                    if ((bool)_uiParentB.ExecuteScript("return $('" + getFullQuery() + "').is(':visible');"))
                    {
                        message = "UI Element found successfully";
                        bFlag = true;
                        break;
                    }                    
                    Thread.Sleep(250);
                }
                stopwatch.Stop();
                Console.WriteLine(message);
                //Console.WriteLine("Element exist: " +this._uiParentB.JQueryWaitForExists(jquery,5000));
                Console.WriteLine("End   ------ looking for: " + jquery + " @ " + DateTime.Now.ToString("HH:mm:ss tt"));
                return bFlag;
            }catch(Exception e)            {
                Console.WriteLine("Exception: " + e.StackTrace);
                return false;
            }
            
        }

        private void prepareXpathJS()
        {
            string content = "";           
            //Open JS file 
            string sPath = CodeUIDriver.getRuntimePath();            
            String projectPath = new Uri(sPath).LocalPath;
            projectPath = projectPath+"src\\main\\resources\\buildXpath.js";
            try
            {
                content = File.ReadAllText(projectPath);
                content += "\r\n return Xpath.getElementTreeXPath($('" + getFullQuery() + "').get()[0]);";                
                sXpath = (string) _uiParentB.ExecuteScript(content);
                //Console.WriteLine("Static xPath: " + rXpath);
            }
            catch(Exception e)
            {                
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }                
        }

        private UITestControl getUIControl()
        {
            
            if (waitForElement(_query))
            {
                prepareXpathJS();
                try 
                {                    
                    HtmlButton uiTestControl = new HtmlButton(Container);
                    var whatever =   uiTestControl.FindFirstByXPath(sXpath);
                    UITestControl uiTestControl2 = UITestControlExtensions.FindFirstByXPath(Container, sXpath);
                    //var oControl = htmlControl.FindFirstByXPath("./"+prepareXpathJS());
                    return whatever;
                }
                catch (Exception e)
                {
                    
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    return null;
                }
            }else
            {
                return null;
            }
           
        }

        #endregion

        /// <summary>
        /// Virtual method so it can be overridden
        /// It will try to find the object
        /// </summary>
        public virtual void click()
        {
            try
            {
                var htmlControl = getUIControl();
                if (htmlControl != null)
                {
                    Mouse.Click(htmlControl);
                    Thread.Sleep(5500);
                }
            }                
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
            }
        }

        private void click(string property)
        {

        }

        protected String getFullQuery()
        {
            string query = _query;
            Component parent = _parent;
            while (parent != null)
            {
                query = parent._query + " " + query;
                parent = parent._parent;
            }
            return query;
        }

        public UITestControl Container
        {
            get { return _uiParentB; }
        }


    }
}
