﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF.src.main.utils
{
    public class ReadResource
    {
        
        
        public class ReadResoure
        { }
        ResourceManager resourceManager = new ResourceManager("CUIATF.src.main.resources.CUIATF", Assembly.GetExecutingAssembly());
        ResourceManager resourceManagerAUT = new ResourceManager("CUIATF.src.test.resources.AUT", Assembly.GetExecutingAssembly());
        
        public String getProperty(String key)
        {
            String value = null;
            try
            {
                value = resourceManager.GetString(key);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while locating the property: " + key);
                Console.WriteLine(ex.Message);
                throw new Exception();
            }
            return value;
        }

        public String getPropertyAUT(String key)
        {
            String value = null;
            try
            {
                value = resourceManagerAUT.GetString(key);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while locating the property: " + key);
                Console.WriteLine(ex.Message);
                throw new Exception();
            }
            return value;
        }       
    }
}
