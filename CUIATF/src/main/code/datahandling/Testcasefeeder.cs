﻿using CUIATF.src.main.utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF.src.main.code.datahandling
{
    public class Testcasefeeder
    {
        private String sheet;
        private static ReadResource resources = new ReadResource();

        public bool feedTestCase(TestContext testContext)
        {
            Boolean bCheck = true;
            String sFlag = "";
            FileStream fs;
            try
            {
                sFlag = testContext.TestName;
                sheet = (sFlag.Substring(0, 3)).ToLower();
                sheet += "_sourcefile_path";
                String temp = resources.getPropertyAUT(sheet);
                if (temp != null)
                {
                    try
                    {
                        String projectPath = new Uri(CodeUIDriver.getRuntimePath()).LocalPath;
                        temp = projectPath + temp;
                        fs = new FileStream(temp, FileMode.OpenOrCreate);
                        fs.Close();
                    }
                    catch (FileNotFoundException e)
                    {
                        Console.WriteLine(e.Message);
                        bCheck = false;
                    }
                }
                else
                {
                    Console.WriteLine("No property found");
                    bCheck = false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return bCheck;
        }

        public String dataSheetProp(TestContext testContext)
        {
            String sFlag = "";
            try
            {
                sFlag = testContext.TestName;
                sheet = (sFlag.Substring(0, 3)).ToLower();
                sheet += "_sourcefile_path";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return sheet;
        }
    }
}
