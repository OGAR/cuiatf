﻿using NLog;
using NPOI.HSSF.UserModel;
using NPOI.Util;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF.src.main.code.datahandling
{
    class ExcelReader
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        private HSSFWorkbook hssfWorkbook = null;
        private Boolean allSheetsDone = false;
        private List<String> sheetNames = new List<string>();

        private Dictionary<String, Dictionary<String, Dictionary<String, String>>> allSheetsMap = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
        private OrderedDictionary od = new OrderedDictionary();

        public List<String> getSheetNames()
        {
            return sheetNames;
        }

        /// <summary>
        /// Expectation is that file is located at the path of the src/test/resources/....
        /// </summary>
        /// <param name="fileName"></param>
        public ExcelReader(String fileName)
        {
            try
            {
                string sPath = CodeUIDriver.getRuntimePath();
                String projectPath = new Uri(sPath).LocalPath;
                fileName = projectPath + fileName;
                FileStream fs = new FileStream(fileName, FileMode.Open);
                hssfWorkbook = new HSSFWorkbook(fs);
                int numberOfSheets = hssfWorkbook.NumberOfSheets;
                for (int i = 0; i < numberOfSheets; i++)
                {
                    sheetNames.Add(hssfWorkbook.GetSheetName(i));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error when trying to open the Data source file: " + fileName);
                logger.Debug("Exception message: " + ex.Message);
            }
        }

        private Dictionary<String, Dictionary<String, Dictionary<String, String>>> convertAllSheetsToMap()
        {

            if (allSheetsDone)
            {
                return allSheetsMap;
            }
            else
            {
                foreach (String sheetName in sheetNames)
                {
                    if (allSheetsMap.ContainsKey(sheetName))
                    {
                        continue;
                    }
                    allSheetsMap.Add(sheetName, convertSheetToMap((HSSFSheet)hssfWorkbook.GetSheet(sheetName)));
                }
                allSheetsDone = true;
            }
            return allSheetsMap;
        }



        private Dictionary<String, Dictionary<String, String>> convertSheetToMap(HSSFSheet sheet)
        {
            Dictionary<String, Dictionary<String, String>> sheetMap = new Dictionary<string, Dictionary<string, string>>();

            int rows = sheet.LastRowNum;
            int cols = sheet.GetRow(0).LastCellNum;

            HSSFRow headerRow = (HSSFRow)sheet.GetRow(0);
            List<String> headerRowNames = new List<String>();

            for (int i = 0; i < cols; i++)
            {
                headerRowNames.Add(headerRow.GetCell(i).StringCellValue);
            }

            for (int i = 1; i <= rows; i++)
            {
                Dictionary<String, String> rowMap = new Dictionary<string, string>();
                HSSFRow dataRow = (HSSFRow)sheet.GetRow(i);
                String key = dataRow.GetCell(0).StringCellValue;
                if (key == null || key.Trim().Length == 0)
                {
                    continue;
                }

                for (int j = 0; j < cols; j++)
                {
                    HSSFCell cell = (HSSFCell)dataRow.GetCell(j);
                    if (cell != null)
                    {
                        rowMap.Add(headerRowNames.ElementAt(j), cell.ToString());
                    }
                    else
                    {
                        rowMap.Add(headerRowNames.ElementAt(j), null);
                    }

                }
                sheetMap.Add(key, rowMap);
            }
            return sheetMap;
        }

        public Dictionary<String, Dictionary<String, String>> getSheetAsMap(String sheetName)
        {
            try
            {
                if (!sheetNames.Contains(sheetName))
                {

                    throw new RuntimeException(sheetName + " not found");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: "+ e.Message);
            }

            Dictionary<String, Dictionary<String, String>> sheetMap = null;
            {
                sheetMap = convertSheetToMap((HSSFSheet)hssfWorkbook.GetSheet(sheetName));
                allSheetsMap.Add(sheetName, sheetMap);
            }

            return sheetMap;
        }

    }
}
