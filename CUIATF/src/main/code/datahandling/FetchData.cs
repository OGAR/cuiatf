﻿using CUIATF.src.main.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUIATF.src.main.code.datahandling
{
    public class FetchData
    {
        private static ReadResource resources = new ReadResource();
        private ExcelReader excelReader = null;

        public FetchData(String filePath)
        {
            excelReader = new ExcelReader(filePath);
        }

        public Dictionary<String, String> mapTestCaseWithData(String testName)
        {
            Dictionary<String, String> dataFeed = null;
            if (excelReader != null)
            {
                // Find the TestCase name match and return the map with data
                for (int i = 0; i < excelReader.getSheetNames().Count; i++)
                {
                    if (excelReader.getSheetNames().ElementAt(i).Equals(resources.getPropertyAUT("aut_datasheet_name")))
                    {
                        Dictionary<String, Dictionary<String, String>> sheetMap = excelReader.getSheetAsMap(excelReader.getSheetNames().ElementAt(i));
                        if (sheetMap.ContainsKey(testName))
                        {
                            dataFeed = sheetMap[testName];
                            break;
                        }
                    }
                }
            }
            return dataFeed;
        }
    }
}
